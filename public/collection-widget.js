

$(document).ready( () => {
    $('.add-another-collection-widget').click(function (e) {
        let list = $($(this).attr('data-list-selector'));
        let counter = list.data('widget-counter') || list.children().length;
        let newWidget = list.attr('data-prototype');
        newWidget = newWidget.replace(/__name__/g, counter);
        newWidget = newWidget.replace(/label__/g, '');
        counter++;
        list.data('widget-counter', counter);
        const newElem = $(list.attr('data-widget-tags')).html(newWidget);
        $(newWidget).appendTo(list);
    });


});

