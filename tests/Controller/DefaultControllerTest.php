<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{
    public function testSomething(): void
    {
        // begin
        $client = static::createClient();
        $client->request('GET', '/');
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Gra o żetony');

        // start game
        $client->request('POST', '/start');
        $response = json_decode($client->getResponse()->getContent(), true);
        $this->assertResponseIsSuccessful();
        $this->assertArrayHasKey('winner', $response);

        $winner = $response['winner'];
        $canTime = $response['canTime'];
        $canTry = $response['canTry'];

        // fail
        $client->request('POST', '/win', ['num' => $winner -1]);
        $response = json_decode($client->getResponse()->getContent(), true);
        $this->assertResponseIsSuccessful();
        $this->assertArrayHasKey('win', $response);
        $this->assertEquals($response['win'], false);

        // success
        $client->request('POST', '/win', ['num' => $winner]);
        $response = json_decode($client->getResponse()->getContent(), true);
        $this->assertResponseIsSuccessful();
        $this->assertArrayHasKey('win', $response);
        $this->assertEquals($response['win'], true);

        // try out
        for ($i = 0; $i <= $canTry - 2; $i++){
            $client->request('POST', '/win', ['num' => $winner]);
        }
        $this->assertResponseStatusCodeSame(500);

        // timeout
        sleep($canTime);
        $client->request('POST', '/win', ['num' => $winner]);
        $this->assertResponseStatusCodeSame(500);
    }
}
