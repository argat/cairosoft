<?php

namespace App\Util;



use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpClient\HttpClient;

class ApiHandler
{
    private $parameterBag;

    function __construct(ParameterBagInterface $parameterBag)
    {
        $this->parameterBag = $parameterBag;
    }

    /**
     * @return \Symfony\Contracts\HttpClient\HttpClientInterface
     */
    function getClient(){
        $token = $this->parameterBag->get('gitToken');
        return HttpClient::create([
            'headers' => [
                "Authorization: token $token"
            ]
        ]);
    }

    /**
     * @return mixed
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    function getList(){
        $response = $this->getClient()->request(
            'GET',
            'https://api.github.com/user/repos'
        );
        return json_decode($response->getContent(), true);
    }
}