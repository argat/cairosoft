<?php

namespace App\Entity;

use App\Repository\ParentEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ParentEntityRepository::class)
 */
class ParentEntity
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=ChildEntity::class, mappedBy="parent", orphanRemoval=true, cascade={"persist"})
     */
    private $childEntities;

    public function __construct()
    {
        $this->childEntities = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, ChildEntity>
     */
    public function getChildEntities(): Collection
    {
        return $this->childEntities;
    }

    public function addChildEntity(ChildEntity $childEntity): self
    {
        if (!$this->childEntities->contains($childEntity)) {
            $this->childEntities[] = $childEntity;
            $childEntity->setParent($this);
        }

        return $this;
    }

    public function removeChildEntity(ChildEntity $childEntity): self
    {
        if ($this->childEntities->removeElement($childEntity)) {
            // set the owning side to null (unless already changed)
            if ($childEntity->getParent() === $this) {
                $childEntity->setParent(null);
            }
        }

        return $this;
    }

    function __toString()
    {
        return $this->name;
    }
}
