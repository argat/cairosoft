<?php

namespace App\Controller;

use App\Entity\ChildEntity;
use App\Entity\ParentEntity;
use App\Form\ParentEntityType;
use App\Repository\ParentEntityRepository;
use App\Type\ChildEntityType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/parent")
 */
class ParentEntityController extends AbstractController
{
    /**
     * @Route("/", name="app_parent_entity_index", methods={"GET"})
     */
    public function index(ParentEntityRepository $parentEntityRepository): Response
    {
        return $this->render('parent_entity/index.html.twig', [
            'parent_entities' => $parentEntityRepository->findAll(),
        ]);
    }

    /**
     * @param $entity
     * @return \Symfony\Component\Form\FormBuilderInterface
     */
    function getFormBuilder($entity){
        return $this->createFormBuilder(
            $entity
        )
            ->add('name', TextType::class)
            ->add(
                'childEntities',
                CollectionType::class,
                [
                    'entry_type' => ChildEntityType::class,
                    'allow_add' => true,
                    'allow_delete' => true
                ]
            );
    }

    /**
     * @param $entity ParentEntity
     */
    function fixParent($entity){
        /** @var  $child ChildEntity */
        foreach($entity->getChildEntities()->toArray() as $child){
            $child->setParent($entity);
        }
    }

    /**
     * @Route("/new", name="app_parent_entity_new", methods={"GET", "POST"})
     */
    public function new(Request $request, ParentEntityRepository $parentEntityRepository): Response
    {
        $parentEntity = new ParentEntity();
        $form = $this->getFormBuilder($parentEntity)->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->fixParent($parentEntity);
            $parentEntityRepository->add($parentEntity);
            return $this->redirectToRoute('app_parent_entity_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('parent_entity/new.html.twig', [
            'parent_entity' => $parentEntity,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_parent_entity_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, ParentEntity $parentEntity, ParentEntityRepository $parentEntityRepository): Response
    {
        $form = $this->getFormBuilder($parentEntity)->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->fixParent($parentEntity);

            $parentEntityRepository->add($parentEntity);

            return $this->redirectToRoute('app_parent_entity_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('parent_entity/edit.html.twig', [
            'parent_entity' => $parentEntity,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_parent_entity_delete", methods={"POST"})
     */
    public function delete(Request $request, ParentEntity $parentEntity, ParentEntityRepository $parentEntityRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$parentEntity->getId(), $request->request->get('_token'))) {
            $parentEntityRepository->remove($parentEntity);
        }

        return $this->redirectToRoute('app_parent_entity_index', [], Response::HTTP_SEE_OTHER);
    }
}
